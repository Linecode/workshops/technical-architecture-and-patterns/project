using Catflix.Commons;
using MediatR;

namespace Catflix.SomeModule.Application;

public record CreateSmth(string Name) : IRequest<Option<Guid>>
{
    private class CreateSmthHandler : IRequestHandler<CreateSmth, Option<Guid>>
    {
        public async Task<Option<Guid>> Handle(CreateSmth request, CancellationToken cancellationToken)
        {
            await Task.Delay(100, cancellationToken);
            
            Console.WriteLine(request.Name);
            
            return new Option<Guid>.Some(Guid.NewGuid());
        }
    }
}
