namespace Catflix.SomeModule;

public abstract class SomeModule
{
}

public static class SomeModuleExtensions 
{
    public static IServiceCollection RegisterSomeModuleServices(this IServiceCollection services)
    {
        return services;
    }
}
