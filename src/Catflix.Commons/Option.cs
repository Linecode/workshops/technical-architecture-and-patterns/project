﻿using Dunet;

namespace Catflix.Commons;

[Union]
public partial record Option<T>
{
    partial record Some(T Value);

    partial record None();
}
