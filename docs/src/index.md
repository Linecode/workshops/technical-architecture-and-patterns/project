# Catflix and Pur

## Skills Matrix

| Capabilities    | Kamil   | Bartosz | Piotr   | Arek    | Jarek   | Wojtek  | Sebastian | Jurek   | Bartek  | Tatiana | Łukasz  |
|-----------------|---------|---------|---------|---------|---------|---------|-----------|---------|---------|---------|---------|
| DDD             | P:3,I:1 | P:1,I:1 | P:1,I:1 | P:2,I:1 | P:2,I:1 | P:1,I:1 | P:1,I:1   | P:0,I:1 | P:2,I:1 | P:0,I:1 | P:2,I:1 |
| .NET Development| P:3,I:0 | P:3,I:1 | P:3,I:0 | P:4,I:1 | P:3,I:1 | P:2,I:1 | P:2,I:1   | P:3,I:1 | P:3,I:1 | P:0,I:1 | P:3,I:1 |
| Message Brokers | P:2,I:1 | P:0,I:1 | P:1,I:1 | P:1,I:1 | P:1,I:1 | P:1,I:1 | P:0,I:1   | P:2,I:1 | P:0,I:1 | P:0,I:1 | P:1,I:1 |
| Cloud           | P:2,I:1 | P:1,I:1 | P:1,I:1 | P:0,I:0 | P:0,I:1 | P:2,I:1 | P:1,I:1   | P:1,I:1 | P:1,I:1 | P:1,I:1 | P:2,I:1 |
| DevOps          | P:1,I:1 | P:0,I:1 | P:1,I:1 | P:2,I:0 | P:1,I:0 | P:1,I:1 | P:1,I:1   | P:1,I:1 | P:2,I:1 | P:1,I:1 | P:1,I:1 |
| EF              | P:3,I:0 | P:0,I:0 | P:1,I:0 | P:3,I:1 | P:1,I:1 | P:1,I:0 | P:1,I:1   | P:1,I:1 | P:1,I:1 | P:0,I:1 | P:2,I:1 |
| Archimate       | P:2,I:0 | P:0,I:0 | P:0,I:0 | P:0,I:0 | P:0,I:1 | P:0,I:0 | P:0,I:1   | P:0,I:1 | P:0,I:1 | P:2,I:1 | P:0,I:0 |
| BDD             | P:2,I:1 | P:0,I:1 | P:1,I:1 | P:1,I:1 | P:1,I:1 | P:0,I:1 | P:0,I:1   | P:0,I:1 | P:1,I:1 | P:0,I:1 | P:0,I:1 |
| Layered Pattern | P:3,I:0 | P:0,I:1 | P:1,I:1 | P:3,I:1 | P:0,I:1 | P:0,I:1 | P:1,I:1   | P:0,I:1 | P:1,I:1 | P:1,I:1 | P:2,I:1 |
| Microservices   | P:3,I:1 | P:1,I:1 | P:1,I:1 | P:0,I:1 | P:1,I:1 | P:0,I:1 | P:1,I:1   | P:1,I:1 | P:1,I:1 | P:1,I:1 | P:1,I:1 |


## Quality Attributes

| Category                    | Details from Script                                                                                                                                            |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Quality Attributes**      |                                                                                                                                                                |
| Performance                 | "Speed and performance are key... The last thing we want is a cat losing interest because a video is buffering."                                               |
| Security                    | "Security is crucial. We don’t want our unique content being pirated or leaked. It’s our competitive edge."                                                    |
| Usability                   | "I've always believed in the mobile-first approach... Our platform should be intuitive and easy to use on these devices."                                      |
| *Availability*              |                                                                                                                                                                |
| Adaptability                | "Localization might be a future concern... We need a system that can adapt and expand based on regional tastes."                                               |
| *Constraints*               |                                                                                                                                                                |
| Budget                      | "I've managed to get $10,000 in AWS credits... We can't afford to go over this budget in our initial phase."                                                   |
| Technology                  | "I'm not too tech-savvy, but I've heard AWS offers a wide range of services... I'd like to make sure we're making the best tech choices right from the start." |
| **Functional Requirements** |                                                                                                                                                                |
| *Content Regionalization*   |                                                                                                                                                                |
| *Content Recommendation*    |                                                                                                                                                                |
| *Content  View*             |                                                                                                                                                                |
| *Content Publish*           |                                                                                                                                                                |
| Platform Support            | "I've always believed in the mobile-first approach... Most of our users will probably use tablets or smartphones to play videos for their cats."               |
| Content Curation            | "In terms of quality, it's not just about high-definition videos... It's about curating content that truly resonates with our feline audience."                |
| User Feedback Loop          | "It's a startup, and agility is crucial. We should be able to quickly test, get feedback, and iterate."                                                        |
| Social Features             | "Have you ever thought about integrating some kind of social feature? Owners might want to share their cats' favorite videos or playlists."                    |
| **Other Influencers**       |                                                                                                                                                                |
| Market Demand               | "We've seen a growing market of products for pets... I believe Catflix and Purr could be the next big thing in this niche."                                    |
| Agility                     | "It's a startup, and agility is crucial. We should be able to quickly test, get feedback, and iterate. The system should allow us to pivot if necessary."      |
| Regional Differences        | "Localization might be a future concern. Cats in Japan might have different content preferences compared to cats in the US."                                   |


## C4

### System Context

@startuml C4_Elements
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(co, "Cat Owner", "Customer of Catflix and Pur")
Person(ccr, "Content Creator", "Optional Description")
Person(ccu, "Content Curator", "Optional Description")
Person(jo, "John", "CEO")
Person(cco, "Cat Coffe Owner", "Optional Description")

System(catflix, "Catflix and Pur", "VOD Streaming Platform")
System_Ext(goog, "Google")
System_Ext(aws, "AWS")
System_Ext(payu, "Payu")

Rel(co, catflix, "uses")
Rel(ccr, catflix, "uses")
Rel(ccu, catflix, "uses")
Rel(jo, catflix, "uses")
Rel(cco, catflix, "uses")
Rel(catflix, goog, "uses")
Rel(catflix, aws, "uses")
Rel(catflix, payu, "uses")
@enduml

### Container

@startuml C4_Elements
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(co, "Cat Owner", "Customer of Catflix and Pur")
Person(ccr, "Content Creator", "Optional Description")
Person(ccu, "Content Curator", "Optional Description")
Person(jo, "John", "CEO")
Person(cco, "Cat Coffe Owner", "Optional Description")

System_Boundary(cat, "Catflix and Pur"){
    Container(web_app, "Web App", "Blazor", "Provides front-end functionality for searching and viewing content and managing user accounts & subscriptions & content administration & curating it")
    Container(mobile_app, "Mobile App", "MAUI", "Provides front-end functionality for searching and viewing content and managing user accounts & subscriptions")
    Container(encoder, "Video Encoder", ".NET", "Backend for Catflix&Purr system")
    Container(api, "Api", ".NET", "Backend for Catflix&Purr system")
    ContainerDb(db, "Databse", "Sqlite")
}

Container_Ext(s3, "AWS S3")
Container_Ext(cf, "AWS Cloudfront")
Container_Ext(pin, "AWS Pinpoint")
Container_Ext(watch, "AWS CloudWatch")
Container_Ext(analytics, "Google Analytics")
Container_Ext(ad, "Google AdSense")
Container_Ext(pay, "PayU")
Container_Ext(cognito, "AWS Cognito")

Rel(ccr, web_app, "uses", "http")
Rel(co, web_app, "uses", "http")
Rel(co, mobile_app, "uses", "http")
Rel(ccu, web_app, "uses", "http")
Rel(jo, web_app, "uses", "http")
Rel(cco, web_app, "uses", "http")

Rel(web_app, analytics, "uses")
Rel(mobile_app, analytics, "uses")
Rel(web_app, api, "uses", "http")
Rel(mobile_app, api, "uses", "http")

Rel(api, cognito, "uses")
Rel(api, pin, "uses")
Rel(api, watch, "uses")
Rel(api, pay, "uses")
Rel(api, db, "uses", "EF")
Rel(db, s3, "uses")

Rel(cf, s3, "import")
Rel(web_app, cf, "uses")
Rel(mobile_app, cf, "uses")
Rel(web_app, ad, "uses")
Rel(mobile_app, ad, "uses")

Rel(encoder, db, "uses", "EF")
Rel(encoder, s3, "uses")
Rel_U(encoder, api, "uses", "amqp")

@enduml

