# ADR

To read more about idea of ADR please visit [adr.github.io](https://adr.github.io/)

Please check ADR template [here](template.html)

| ID    | Name                                                                              | Author                            | Status |
|-------|-----------------------------------------------------------------------------------|-----------------------------------| ------ |
| 00001 | [example](template.html)                   | Kamil Kiełbasa | ![](https://img.shields.io/badge/ZAAKCEPTOWANA-green.svg?style=flat-square)  |

