# Catflix and Pur


## Documentation

On newer version of Node.js you need to set this env variable.

### Linux / Mac

```bash
export NODE_OPTIONS=--openssl-legacy-providerbash
```

### Windows

```bash
SET NODE_OPTIONS=--openssl-legacy-provider
```

